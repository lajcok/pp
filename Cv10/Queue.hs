module Queue where

    data Queue a = Item a (Queue a) | End
        deriving Show
        
    isEmpty :: Queue a -> Bool
    isEmpty End = True
    isEmpty _ = False

    -- O(n)
    add :: a -> Queue a -> Queue a
    add e End = Item e End
    add e (Item i q) = Item i (add e q)

    -- O(1)
    remove :: Queue a -> (a, Queue a)
    remove (Item i q) = (i, q)
    
    queue = foldl (flip (add)) End [1..10]
