module Stack where

    data Stack a = Elem a (Stack a) | Bed
        deriving Show

    push :: a -> Stack a -> Stack a
    push e s = Elem e s

    pop :: Stack a -> Stack a
    pop (Elem _ s) = s

    top :: Stack a -> a
    top (Elem e _) = e

    isEmpty :: Stack a -> Bool
    isEmpty Bed = True
    isEmpty _ = False
    
    stack = foldl (flip (push)) Bed [1..10]
