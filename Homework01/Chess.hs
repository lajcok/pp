module Homework01.Chess where

    import Data.Char

    type Result = [[Char]]

    pp :: Result -> IO ()
    pp x = putStr $ concat $ map (++"\n") x

    rows = map (intToDigit) [8,7..1]
    cols = ['a'..'h']

    chess :: [String] -> [String] -> Result
    chess w b = [[
            field c r | c <- ' ':cols
        ] | r <- rows ++ " "]
        where
            field c ' ' = c
            field ' ' r = r
            field c r | length wh > 0 = head $ head wh
                      | length bl > 0 = toLower $ head $ head bl
                      | otherwise = '.'
                        where
                            flt = filter (\f -> c:r:"" == tail f)
                            wh = flt w
                            bl = flt b

    playField :: Result -> Result
    playField board = map (tail) (take (length rows) board)

    whiteOnlyRowsCount :: Result -> Int
    whiteOnlyRowsCount board = count (\(w, b) -> b <= 0 && w > 0) rowCounts
        where
            rowCounts = [
                    (count (isUpper) r, count (isLower) r)
                    | r <- playField board
                ]
            count cond fields = length $ filter (cond) fields


    isMoveLegal :: Result -> Result -> Bool
    isMoveLegal boardA boardB = legal $ move diff
        where
            diff = [
                (c, r, fA, fB) |
                    (r, rA, rB) <- zip3 rows (playField boardA) (playField boardB),
                    (c, fA, fB) <- zip3 cols rA rB,
                    fA /= fB
                ]
            move [(c1, r1, fA1, fB1), (c2, r2, fA2, fB2)]
                | fA1 == fB2 && fB1 == '.' = (fA1, c1, r1, fA2, c2, r2)
                | fA2 == fB1 && fB2 == '.' = (fA2, c2, r2, fA1, c1, r1)
            legal (figOrig, c1, r1, spot, c2, r2) | isClean = case toUpper figOrig of
                    'K' -> abs cDiff <= 1 && abs rDiff <= 1
                    'Q' -> cDiff == 0 || rDiff == 0 || abs cDiff == abs rDiff
                    'R' -> cDiff == 0 || rDiff == 0
                    'B' -> abs cDiff == abs rDiff
                    'N' -> let absDiff = (abs cDiff, abs rDiff) in absDiff == (2,3) || absDiff == (3,2)
                    'P' -> let isWhite = isUpper figOrig
                               rStep = if isWhite then -rDiff else rDiff
                               advance = r1 == (if isWhite then '2' else '7') && cDiff == 0 && 1 <= rStep && rStep <= 2
                               attack = abs cDiff == 1 && rStep == 1
                           in if spot == '.' then advance else attack
                where
                    selfAttack = spot /= '.' && isUpper figOrig == isUpper spot
                    isClean = not selfAttack
                    rn = digitToInt
                    cDiff = ord c1 - ord c2
                    rDiff = rn r1 - rn r2


            

            

