module Cv07 where
    
    type Pic = [[Char]]

    pp :: Pic -> IO ()
    pp x = putStr $ concat $ map (++"\n") x

    pic :: Pic
    pic = [ "....#....",
            "...###...",
            "..#.#.#..",
            ".#..#..#.",
            "....#....",
            "....#....",
            "....#####"]

    flipH :: Pic -> Pic
    flipH = reverse

    flipV :: Pic -> Pic
    flipV = map reverse

    above :: Pic -> Pic -> Pic
    above = (++)

    sideBySide :: Pic -> Pic -> Pic
    sideBySide = zipWith (++)

    rotateR :: Pic -> Pic
    rotateR xss = foldl1 (sideBySide) [[[x] | x <- xs] | xs <- reverse xss]

    rotateL :: Pic -> Pic
    rotateL xss = foldl1 (sideBySide) [[[x] | x <- (reverse xs)] | xs <- xss]

    zoom :: (Integral a) => a -> Pic -> Pic -- using generator
    zoom n p | n > 0 = [[c | c <- row, _ <- [1..n]] | row <- p, _ <- [1..n]]

    zoom' :: Int -> Pic -> Pic -- using replication
    zoom' n p | n > 0 = concat [replicate n (concat [replicate n c | c <- row]) | row <- p]
