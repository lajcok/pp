module Lists where

    length' :: (Integral b) => [a] -> b
    length' [] = 0
    length' (_:xs) = 1 + length' xs

    sum' :: (Integral a) => [a] -> a
    sum' [] = 0
    sum' (x:xs) = x + sum' xs

    head' :: [a] -> a
    head' (x:xs) = x

    last' :: [a] -> a
    last' [x] = x
    last' (_:xs) = last'(xs)

    in' :: (Eq a) => a -> [a] -> Bool
    in' e [] = False
    in' e (x:xs) = if e == x then True else e `in'` xs

    tail' :: [a] -> [a]
    tail' (_:xs) = xs

    init' :: [a] -> [a]
    init' [x] = []
    init' (x:xs) = x : init' xs

    combine' :: [a] -> [a] -> [a]
    combine' [] ys = ys
    combine' (x:xs) ys = x : combine' xs ys

    max' :: (Ord a) => [a] -> a
    max' [x] = x
    max' (x:xs) = if x > mx then x else mx
        where mx = max' xs

    reverse' :: [a] -> [a] -- O(n^2)
    reverse' [] = []
    reverse' (x:xs) = (reverse' xs) `combine'` [x]

    reverse'' :: [a] -> [a] -- O(n)
    reverse'' xs = tmp xs []
        where tmp [] ys = ys
              tmp (x:xs) ys = tmp xs (x:ys)

    scalar' :: (Real a) => [a] -> [a] -> a
    scalar' [x] [y] = x * y
    scalar' (x:xs) (y:ys) = [x] `scalar'` [y] + xs `scalar'` ys
