module Functions where

    hello :: String -> String
    hello a = "Hello, " ++ a ++ "!"
        
    pythagoras :: (Floating a) => a -> a -> a
    pythagoras a b = sqrt (a^^2+b^^2)

    fact :: (Integral a) => a -> a
    fact 0 = 1
    fact a | a > 0 = a * fact (a - 1)

    fib :: (Integral a) => a -> a
    fib 0 = 0
    fib 1 = 1
    fib n = tmp n 0 1
    --fib n | n > 1 = fib (n-1) + fib (n-2)
    tmp 1 a b = b
    tmp n a b = tmp (n-1) b (a+b)

    gcd' :: (Integral a) => a -> a -> a
    gcd' a b | a == b = a
             | otherwise = gcd' (mx-mn) mn
             where mx = max a b
                   mn = min a b

    isPrime :: Int -> Bool
    isPrime 1 = False
    isPrime n = isPrimeTest n (ceiling $ sqrt $ fromIntegral (n-1))
    isPrimeTest n 1 = True
    isPrimeTest n x | n `mod` x == 0 = False
                    | otherwise = isPrimeTest n (x-1)

