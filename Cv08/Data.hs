module DataTypes where

    data Expr = Num Int
                | Add Expr Expr
                | Sub Expr Expr
                | Mul Expr Expr
                | Div Expr Expr
                | Var Char
            deriving (Eq)

    test1 :: Expr
    test1 = Add (Num 1) (Num 2)
    test2 :: Expr
    test2 = Mul (Add (Num 1) (Num 20)) (Num 3)
    test3 :: Expr
    test3 = Mul (Num 3) (Var 'c')

    eval :: Expr -> Int
    eval (Num x) = x
    eval (Add x y) = eval x + eval y
    eval (Sub x y) = eval x - eval y
    eval (Mul x y) = eval x * eval y
    eval (Div x y) = eval x `div` eval y

    showExpr :: Expr -> String
    showExpr x = showExpr' x False
        where
            showExpr' (Num x) _ = show x
            showExpr' (Add x y) hgh = let res = showExpr' x False ++ " + " ++ showExpr' y False
                                      in if hgh then brace res else res
            showExpr' (Sub x y) hgh = let res = showExpr' x False ++ " - " ++ showExpr' y False
                                      in if hgh then brace res else res
            showExpr' (Mul x y) _ = showExpr' x True ++ " * " ++ showExpr' y True
            showExpr' (Div x y) _ = showExpr' x True ++ " / " ++ showExpr' y True
            showExpr' (Var x) _ = [x]
            brace x = "(" ++ x ++ ")"

    instance Show Expr where
        show = showExpr

    d' :: Expr -> Expr -> Expr
    d' (Var x) expr = d'' x expr
        where
            d'' _ (Num a) = Num 0
            d'' x (Var a) = if a == x then Num 1 else Num 0
            d'' x (Add a b) = Add (d'' x a) (d'' x b)
            d'' x (Sub a b) = Sub (d'' x a) (d'' x b)
            d'' x (Mul a b) = Add (Mul (d'' x a) b) (Mul a (d'' x b))
            d'' x (Div a b) = Div (Sub (Mul (d'' x a) b) (Mul a (d'' x b))) (Mul b b)
