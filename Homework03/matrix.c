#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "malloc.h"
#include "omp.h"

int** newMatrix(int n);
void fillRandom(int low, int high, int n, int **m);
void print(int n, int **m);
void multiply(int n, int **a, int **b, int **out);

int main(int args, char **argv)
{
	int n = 0, **a, **b, **out;

	if (args >= 2) {
		n = strtol(argv[1], NULL, 10);
	}
	
	if (n <= 0) {
		n = 2;
	}

	printf("n=%d\n", n);

	a = newMatrix(n);
	b = newMatrix(n);
	out = newMatrix(n);

	srand(time(NULL));
	fillRandom(-10, +10, n, a);
	fillRandom(-10, +10, n, b);

	printf("A=\n");
	print(n, a);
	printf("B=\n");
	print(n, b);

	multiply(n, a, b, out);
	printf("A*B=\n");
	print(n, out);

	free(a);
	free(b);
	free(out);

	return 0;
}

int** newMatrix(int n) {
	int *ptr, **m, i;

	m = (int**)malloc(sizeof(int*) * n + sizeof(int) * n * n);
	ptr = (int *)(m + n);

	for (i = 0; i < n; ++i) {
		m[i] = ptr + n * i;
	}

	return m;
}

void fillRandom(int low, int high, int n, int **m) {
	int i, j;
	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			m[i][j] = rand() % (high - low + 1) + low;
		}
	}
}

void print(int n, int **m)
{
	if (n <= 10) {
		int i, j;
		for (i = 0; i < n; ++i)
		{
			for (j = 0; j < n; ++j)
			{
				printf("%4d", m[i][j]);
			}
			printf("\n");
		}
	}
	else {
		printf("%dx%d\n", n, n);
	}
}

void multiply(int n, int **a, int **b, int **out)
{
	int i, j;
	#pragma omp parallel for
	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			int k;
			out[i][j] = 0;
			for (k = 0; k < n; ++k)
			{
				out[i][j] += a[i][k] * b[k][j];
			}
		}
	}
}
