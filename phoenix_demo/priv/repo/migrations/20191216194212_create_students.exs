defmodule PhoenixDemo.Repo.Migrations.CreateStudents do
  use Ecto.Migration

  def change do
    create table(:students) do
      add :login, :string
      add :name, :string
      add :points, :integer

      timestamps()
    end

  end
end
