defmodule PhoenixDemo.Student do
  use Ecto.Schema
  import Ecto.Changeset

  schema "students" do
    field :login, :string
    field :name, :string
    field :points, :integer

    timestamps()
  end

  @doc false
  def changeset(student, attrs) do
    student
    |> cast(attrs, [:login, :name, :points])
    |> validate_required([:login, :name])
    |> validate_length(:login, is: 7)
    |> validate_number(:points, greater_than_or_equal_to: 0)
  end
end
