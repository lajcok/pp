defmodule PhoenixDemoWeb.StudentController do
    use PhoenixDemoWeb, :controller

    import Routes

    alias PhoenixDemo.Repo
    alias PhoenixDemo.Student

    def index(conn, _params) do
        import Ecto.Query
        render conn, "index.html", students: Repo.all(from Student, order_by: :id)
    end

    def new(conn, _params) do
        student = %Student{}
        render conn, "show.html",
            student: student,
            changeset: Student.changeset(student, %{}),
            action: student_path(conn, :create)
    end

    def create(conn, %{"student" => data}) do
        %Student{}
        |> Student.changeset(data)
        |> Repo.insert()
        |> case do
            {:ok, ...} ->
                redirect(conn, to: student_path(conn, :index))
            {:error, %Ecto.Changeset{} = changeset} ->
                render conn, "show.html",
                    student: %Student{},
                    changeset: changeset,
                    action: student_path(conn, :create)
        end
    end

    def show(conn, %{"id" => id}) do
        student = Repo.get!(Student, id)
        render conn, "show.html", id: id,
            student: student,
            changeset: Student.changeset(student, %{}),
            action: student_path(conn, :update, id)
    end

    def update(conn, %{"id" => id, "student" => data}) do
        Repo.get!(Student, id)
        |> Student.changeset(data)
        |> Repo.update()
        |> case do
            {:ok, ...} ->
                redirect(conn, to: student_path(conn, :index))
            {:error, %Ecto.Changeset{} = changeset} ->
                render conn, "show.html", changeset: changeset,
                    action: student_path(conn, :update, id)
        end
    end

    def delete(conn, %{"id" => id}) do
        Repo.get!(Student, id)
        |> Repo.delete()
        redirect conn, to: student_path(conn, :index)
    end

end