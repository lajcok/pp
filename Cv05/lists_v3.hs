module Cv05 where

    countThem :: String -> [(Char, Int)]
    countThem (c:str) = (c, count + 1) : countThem (filter (/=c) str) where
        count = length $ filter (==c) str
    countThem _ = []
    
    countThem' :: String -> [(Char, Int)]
    countThem' str = [(uq, length $ filter (==uq) str) | uq <- uqs] where
        uqs = unique (reverse str) []
        unique [] uqs = uqs
        unique (x:xs) uqs | x `elem` uqs = unique xs uqs
                          | otherwise = unique xs (x:uqs)

    --goldbach :: Int-> [(Int, Int)] -- TODO as homework
    --goldbach n | n > 2 =  where
    --    primes = [x | x <- [1..n], isPrime x]
    --    isPrime a = null [x | x <- [2..ceiling (sqrt n)], a `mod` x == 0]


    -- combinations -- TODO as homework
