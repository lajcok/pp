module Homework02.RegularLanguage where

    -- Assignment:
    --  ☑ Regular expression data structure
    --  ☑ Convert to finite automaton
    --  ☑ ... and then to deterministic
    --  ☑ Deterministic finite automaton evaluation

    data Atomic = Epsilon | Literal Char deriving (Show, Eq)
    data Expression = Empty
                    | Atomic Atomic
                    | Concat Expression Expression
                    | Alter Expression Expression
                    | Star Expression
        deriving (Show)

    type Transition = (Atomic, FA)
    data AR = Accept | Reject deriving (Show, Eq)
    data FA = State AR [Transition] deriving (Show)

    (&&&) :: AR -> AR -> AR
    (&&&) Accept Accept = Accept
    (&&&) _ _ = Reject
    infix 3 &&&

    (|||) :: AR -> AR -> AR
    (|||) Reject Reject = Reject
    (|||) _ _ = Accept
    infix 2 |||


    join :: FA -> FA -> FA
    join (State ar ts) fa2 = State Reject prep where
        exts = map (\(a, fa) -> (a, join fa fa2)) ts
        prep = if ar == Accept then (Epsilon, fa2):exts else exts


    merge :: FA -> FA -> FA
    merge (State ar1 ts1) (State ar2 ts2) = State (ar1 ||| ar2) (ts1 ++ ts2)


    asFA :: Expression -> FA
    asFA Empty = State Reject []
    asFA (Atomic a) = State Reject [(a, State Accept [])]
    asFA (Concat e1 e2) = join (asFA e1) (asFA e2)
    asFA (Alter e1 e2) = State Reject [(Epsilon, asFA e1), (Epsilon, asFA e2)]
    asFA (Star e) = let current = State Accept [(Epsilon, join (asFA e) current)] in current


    removeEpsilon :: FA -> FA
    removeEpsilon (State ar ts) = foldr (merge) (State ar nonEpsilon) epsilon where
        nets = map (\(a, fa) -> (a, removeEpsilon fa)) ts
        epsilonClasses = foldr (classifier) ([], []) nets
        classifier (a, fa) (e, n) = if a == Epsilon then (fa:e, n) else (e, (a, fa):n)
        epsilon = fst epsilonClasses
        nonEpsilon = snd epsilonClasses


    toDFA :: [Char] -> FA -> FA
    toDFA az (State ar ts) = State ar transitions where
        transitions = [(Literal c, toDFA az $ mergeAll $ matching c) | c <- az]
        matching c = [fa | (Literal l, fa) <- ts, l == c]
        mergeAll = foldr (merge) (State Reject [])


    asDFA :: [Char] -> Expression -> FA
    asDFA az = toDFA az . removeEpsilon . asFA

    
    check :: FA -> String -> AR
    check (State ar _) [] = ar
    check (State _ ts) (c:cs) = check (select ts) cs where
        select ((Literal l, fa):_t) = if l == c then fa else select _t
        select _ = error ("Cannot find transition " ++ (show c) ++ ", probably not DFA")
