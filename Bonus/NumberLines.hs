module NumberLines where

    numberLines :: [String] -> [String]
    numberLines ls = [(show n) ++ ' ':l | (n, l) <- zip [1..length ls] ls]

    main = do
        putStr "Path: "
        path <- getLine
        file <- readFile path
        writeFile ("n_" ++ path) [c | l <- numberLines $ lines file, c <- l ++ "\n"]
         