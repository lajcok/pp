module ListsV2 where
    import Data.Char

    take' :: (Integral n) => n -> [a] -> [a]
    take' 0 as = []
    take' n [] | n > 0 = []
    take' n (a:as) = a : take' (n-1) as

    drop' :: (Integral n) => n -> [a] -> [a]
    drop' 0 as = as
    drop' n [] | n > 0 = []
    drop' n (_:as) = drop' (n-1) as

    min' :: (Ord x) => [x] -> x
    min' [x] = x
    min' (x:xs) = if x < mn then x else mn
        where mn = min' xs

    divisors :: (Integral x) => x -> [x]
    divisors n | n >= 1 = tmp n
        where tmp 1 = [1]
              tmp x = if n `mod` x == 0
                        then x : next
                        else next
                where next = tmp (x-1)

    divisors' :: (Integral x) => x -> [x] -- using filter
    divisors' n | n >= 1 = filter (\x -> n `mod` x == 0) [n, n-1 .. 1]

    divisors'' :: (Integral x) => x -> [x] -- using comprehension
    divisors'' n | n >= 1 = [x | x <- [n, n-1 .. 1], n `mod` x == 0]

    zip' :: [a] -> [b] -> [(a,b)]
    zip' (x:xs) (y:ys) = (x,y) : zip xs ys
    zip' _ _ = []

    reverse' :: [a] -> [a] -- using foldl
    reverse' = foldl (\acc x -> x : acc) []

    cartesian :: [a] -> [b] -> [(a,b)]
    cartesian (x:xs) ys = cart ys ++ cartesian xs ys where
        cart [] = []
        cart (y:ys) = (x,y) : cart ys
    cartesian _ _ = []
    
    cartesian' :: [a] -> [b] -> [(a,b)] -- using map
    cartesian' (x:xs) ys = map (\y -> (x,y)) ys ++ cartesian xs ys
    cartesian' _ _ = []

    cartesian'' :: [a] -> [b] -> [(a,b)] -- using generator
    cartesian'' xs ys = [(x,y) | x <- xs, y <- ys]

    -- cartesian''' -- for tuples TODO as homework

    allToUpper :: String -> String
    allToUpper s = map toUpper s

    quicksort :: (Ord a) => [a] -> [a]
    quicksort (x:xs) = quicksort (lower xs) ++ [x] ++ quicksort (higher xs) where
        lower = filter (<x)
        higher = filter (>=x)
    quicksort _ = []


