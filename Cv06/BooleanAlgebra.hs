module Cv06 where

    not' :: Bool -> Bool
    not' True = False
    not' False = True
    infixl 5 `not'`

    and' :: Bool -> Bool -> Bool
    and' True True = True
    and' _ _ = False
    infixr 3 `and'`

    or' :: Bool -> Bool -> Bool
    or' False False = False
    or' _ _ = True
    infixr 2 `or'`

    nand' :: Bool -> Bool -> Bool
    nand' x y = not' (and' x y)
    infixr 3 `nand'`

    xor' :: Bool -> Bool -> Bool
    xor' x y = x /= y
    infixr 2 `xor'`

    impl' :: Bool -> Bool -> Bool
    impl' True False = False
    impl' _ _ = True
    infixr 6 `impl'`

    equ' :: Bool -> Bool -> Bool
    equ' x y = x == y
    infixr 4 `equ'`

    table :: (Bool -> Bool -> Bool) -> IO ()
    table expr = putStr $ concat $ map (line) comb
                 where
                    comb = [(x, y, expr x y) | x <- [True, False], y <- [True, False]]
                    line (x, y, r) = (asCh x):' ':(asCh y):" | " ++ (asCh r):"\n"
                    asCh b = if b then '1' else '0'
                    

    table' :: Int -> ([Bool] -> Bool) -> IO ()
    table' n expr | n >= 0 = putStr $ concat $ map (line) $ [(xs, expr xs) | xs <- comb n]
        where
            comb 0 = [[]]
            comb i | i > 0 = [x:xs | xs <- comb (i-1), x <- [True, False]]
            line (xs, r) = concat [asCh x:" " | x <- xs] ++ "| " ++ (asCh r):"\n"
            asCh b = if b then '1' else '0'

